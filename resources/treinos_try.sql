select * 
from mesociclos
where id_aluno = 410;


select t.data, t.treino, t.volume, m.id, m.periodo
from treinos t
join mesociclos m on (m.id_aluno = t.id_aluno)
where t.id_aluno = 410
and (t.volume > 0 or t.treino <> '')
-- and (month(t.data) = month(m.data) and year(t.data) = year(m.data))
and t.data >= m.data
and t.data <
(
	select m1.data
	from mesociclos m1
	where m1.id_aluno = t.id_aluno
	and m1.id >= m.id
	and m1.data > t.data
	order by m.data desc
	limit 0,1
)
-- and m.id = 4060
-- and m.id = 3465
-- and m.id = 4060
order by data asc

;