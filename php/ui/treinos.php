
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Winners - Treinos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Eduardo Martins">

    <link rel="stylesheet" href="jquery/jquery.mobile-1.3.1.css" />
    <link rel="stylesheet" href="jquery/jquery.mobile.structure-1.3.1.css" />
    <link rel="stylesheet" href="jquery/themes/winners.css" />

    <link rel="stylesheet" href="css/winners.css" />

    <script src="jquery/jquery-1.9.1.js"></script>
    <script src="jquery/jquery.mobile-1.3.1.js"></script>

  </head>

  <body>

    <div data-role="page">

        <div data-role="panel" id="mypanel" data-position="right" data-position-fixed="true" data-display="push">
            <ul data-role="listview">
                <li data-role="list-divider">Mesociclos</li>
                <li data-icon="false"><a href="#treino" data-rel="close">Mesociclo 1</a></li>
                <li data-icon="false"><a href="#treino" data-rel="close">Mesociclo 2</a></li>
                <li data-icon="false"><a href="#treino" data-rel="close">Mesociclo 3</a></li>
                <li data-icon="false"><a href="#treino" data-rel="close">Mesociclo 4</a></li>
                <li data-icon="false"><a href="#treino" data-rel="close">Mesociclo 5</a></li>
                <li data-icon="false"><a href="#treino" data-rel="close">Mesociclo 6</a></li>
                <li data-icon="false"><a href="#treino" data-rel="close">Mesociclo 7</a></li>
            </ul>
        </div><!-- /panel -->


        <div data-role="header" data-position="fixed">
            <a href="home.html" data-rel="back" data-icon="arrow-l" data-iconpos="notext" data-corners="false" data-inline="true">voltar</a> 
            <h1>Treinos</h1>
            <a href="#mypanel" data-icon="bars" data-iconpos="notext" data-corners="false" data-inline="true">mesociclos</a> 
        </div>

        <div data-role="content" id="treino">
            <h2>Mesociclo 3</h1>
            <ul data-role="listview" data-inset="true">
                <li data-role="list-divider">Terça, 21 de Maio de 2013</li>
                <li>
                    <h2>Rua</h2><br>
                    <p>2km trote</p>
                    <p>+2x5x200m (53-56s)</p>
                    <p>P - 200m de trote</p>
                    <p>MP - 3'</p>
                    <p>2km de trote</p>
                    <p class="ui-li-aside"><strong>19:30</strong></p>
                </li>
                <li data-role="list-divider">Terça, 23 de Maio de 2013</li>
                <li>
                    <h2>CETE</h2><br>
                    <p>2km trote</p>
                    <p>+ 3 x 3 x 400m (2'00-05 por volta)</p>
                    <p>P - 1'30"</p>
                    <p>MP - 3'</p>
                    <p>Final: 2km de trote</p>
                    <p class="ui-li-aside"><strong>19:30</strong></p>
                </li>
                <li data-role="list-divider">Terça, 25 de Maio de 2013</li>
                <li>
                    <h2>Equipe</h2><br>
                    <p>12km de rodagem</p>
                    <p class="ui-li-aside"><strong>08:00</strong></p>
                </li>
            </ul>
        </div><!-- /content -->
    </div><!-- page -->
</html>