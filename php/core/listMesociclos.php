<?php

require_once("config.properties.php");
require_once(ADODB);
require_once(UTIL."Util.class.php");
require_once(SERVICE."MesocicloService.class.php");

if (isset($_REQUEST["id"])) {
	$id = $_REQUEST["id"];

	$mesocicloService = new MesocicloService();
	$ret = $mesocicloService->listMesociclosByAluno($id);

	$str = json_encode($ret);

	header('Content-Type: application/json');
	echo json_encode($ret);
} else {
	echo "server error";
}

?>