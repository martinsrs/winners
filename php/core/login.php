<?php

require_once("config.properties.php");
require_once(ADODB);
require_once(UTIL."Util.class.php");
require_once(SERVICE."AlunoService.class.php");

$ret = null;

if (isset($_REQUEST["usr"]) && isset($_REQUEST["pss"])) {

	$user = $_REQUEST["usr"];
	$pass = $_REQUEST["pss"];

	$alunoService = new AlunoService();
	$login =  $alunoService->login($user, $pass);
	
	if ($login) {
		$ret = $alunoService->listAlunoById($login);
		
	} else {
		$ret = array('err'=>'Usuário e/ou Senha inválidos');
	}
} else {
	$ret = array('err'=>'erro ao conectar ao aplicativo');
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($ret);

?>