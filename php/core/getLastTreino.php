<?php

require_once("config.properties.php");
require_once(ADODB);
require_once(UTIL."Util.class.php");
require_once(SERVICE."TreinoService.class.php");

$ret = null;

if (isset($_REQUEST["usr"])) {

	$user = $_REQUEST["usr"];

	$treinoService = new TreinoService();
	$ret = $treinoService->getLastTreinoByAluno($user);
	
} else {
	$ret = array('err'=>'erro ao conectar ao aplicativo');
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($ret);

?>