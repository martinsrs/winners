<?php

require_once("config.properties.php");
require_once(ADODB);
require_once(UTIL."Util.class.php");
require_once(SERVICE."MesocicloService.class.php");

$ret = null;
$mesocicloService = new MesocicloService();

if (isset($_REQUEST["usr"])) {

	$user = $_REQUEST["usr"];
	$ret = $mesocicloService->listMesociclosByAluno($user);
	
} else if (isset($_REQUEST["id"])) {
	
	$id = $_REQUEST["id"];
	$ret = $mesocicloService->listMesociclosById($id);

} else {
	$ret = array('err'=>'erro ao conectar ao aplicativo');
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($ret);

?>