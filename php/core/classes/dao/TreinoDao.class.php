<?php
require_once ("Connection.class.php");
require_once (VO."Treino.class.php");

class TreinoDao {

	private $SQL_GET_ALL_TREINOS_BY_ALUNO = "select (week(t.data,1)-week(m.data,1))+1 as numSemana, m.id as idMesociclo, t.id, t.id_aluno, t.data, t.modalidade, t.treino, t.volume, t.feedback
												,case weekday(t.data)
													when 0 then a.segObs
													when 1 then a.terObs
													when 2 then a.quaObs
													when 3 then a.quiObs
													when 4 then a.sexObs
													when 5 then a.sabObs
													else a.domObs
												end as obs
											from treinos t 
											join mesociclos m
											join alunos a on (a.id = t.id_aluno)
											where t.id_aluno = ?
											and t.id_aluno = m.id_aluno
											and t.data >= m.data
											and t.data <= date_add(date_sub(m.data, interval 1 day), interval 4 week)
											and t.volume > 0 
											and treino <> ''
											order by t.data";
	
	private $SQL_GET_ALL_TREINOS_BY_ALUNO_MESOCICLO = "select (week(t.data,1)-week(m.data,1))+1 as numSemana, m.id as idMesociclo, t.id, t.id_aluno, t.data, t.modalidade, t.treino, t.volume, t.feedback
												,case weekday(t.data)
													when 0 then a.segObs
													when 1 then a.terObs
													when 2 then a.quaObs
													when 3 then a.quiObs
													when 4 then a.sexObs
													when 5 then a.sabObs
													else a.domObs
												end as obs
											from treinos t 
											join mesociclos m
											join alunos a on (a.id = t.id_aluno)
											where t.id_aluno = ?
											and m.id = ?
											and t.id_aluno = m.id_aluno
											and t.data >= m.data
											and t.data <= date_add(date_sub(m.data, interval 1 day), interval 4 week)
											and t.volume > 0 
											and treino <> ''
											order by t.data";

	private $SQL_GET_LAST_TREINO_BY_ALUNO = "select (week(t.data,1)-week(m.data,1))+1 as numSemana, m.id as idMesociclo, t.id, t.id_aluno, t.data, t.modalidade, t.treino, t.volume, t.feedback
												,case weekday(t.data)
													when 0 then a.segObs
													when 1 then a.terObs
													when 2 then a.quaObs
													when 3 then a.quiObs
													when 4 then a.sexObs
													when 5 then a.sabObs
													else a.domObs
												end as obs
											from treinos t 
											join mesociclos m
											join alunos a on (a.id = t.id_aluno)
											where t.id_aluno = ?
											and t.id_aluno = m.id_aluno
											and t.data >= m.data
											and t.data <= date_add(date_sub(m.data, interval 1 day), interval 4 week)
											and t.volume > 0 
											and treino <> ''
											and t.data >= sysdate()
											order by t.data
											limit 0,1";

	private $SQL_GET_LAST_TREINO_BY_ALUNO2 = "select (week(t.data,1)-week(m.data,1))+1 as numSemana, m.id as idMesociclo, t.id, t.id_aluno, t.data, t.modalidade, t.treino, t.volume, t.feedback
												,case weekday(t.data)
													when 0 then a.segObs
													when 1 then a.terObs
													when 2 then a.quaObs
													when 3 then a.quiObs
													when 4 then a.sexObs
													when 5 then a.sabObs
													else a.domObs
												end as obs
											from treinos t 
											join mesociclos m
											join alunos a on (a.id = t.id_aluno)
											where t.id_aluno = ?
											and t.id_aluno = m.id_aluno
											and t.data >= m.data
											and t.data <= date_add(date_sub(m.data, interval 1 day), interval 4 week)
											and t.volume > 0 
											and treino <> ''
											order by t.data desc
											limit 0,1";


	private $SQL_UPDATE_FEEDBACK = " update treinos set feedback = ? where id = ? ";									

	private function populateVO($rs, &$arrTreinos) {
		
		$arrTreinos = array();
		while (!$rs->EOF) {

			$arr = $rs->fetchRow();

			$treino = new Treino();
			$treino->setIdMesociclo( $arr["idMesociclo"] );
			$treino->setNumSemana( $arr["numSemana"] );
			$treino->setId( $arr["id"] );
			$treino->setIdAluno( $arr["id_aluno"] );
			$treino->setData( $arr["data"] );
			$treino->setModalidade( $arr["modalidade"] );
			$treino->setTreino( $arr["treino"] );
			$treino->setVolume( $arr["volume"] );
			$treino->setFeedback( $arr["feedback"] );
			$treino->setObs( $arr["obs"] );

			array_push($arrTreinos, $treino);
		}

		return $arrTreinos;
	}

	public function listTreinosByAluno($idAluno) {
		$conn = new Connection();

		$rs = $conn->executeQryP($this->SQL_GET_ALL_TREINOS_BY_ALUNO, array($idAluno));
		$this->populateVO($rs, $arrTreinos); 
		return $arrTreinos;
	}

	public function listTreinosByAlunoMesociclo($idAluno, $idMesociclo) {
		$conn = new Connection();

		$rs = $conn->executeQryP($this->SQL_GET_ALL_TREINOS_BY_ALUNO_MESOCICLO, array($idAluno, $idMesociclo));

		$arrTreinos = array();
		$this->populateVO($rs, $arrTreinos); 
		return $arrTreinos;
	}

	public function getLastTreinoByAluno($idAluno) {
		$conn = new Connection();

		$rs = $conn->executeQryP($this->SQL_GET_LAST_TREINO_BY_ALUNO, array($idAluno));

		if ($rs->EOF) {
			$rs = $conn->executeQryP($this->SQL_GET_LAST_TREINO_BY_ALUNO2, array($idAluno));
		}

		$arrTreinos = array();
		$this->populateVO($rs, $arrTreinos); 
		return $arrTreinos;
	}

	public function updateFeedBack($id, $feedback) {
		$conn = new Connection();
		return $conn->executeQryP($this->SQL_UPDATE_FEEDBACK, array($feedback, $id));
	}

}

?>