<?php

require_once ("Connection.class.php");
require_once (VO."Mesociclo.class.php");

class MesocicloDao {
	private $SQL_LIST_MESOCICLOS_BY_ALUNO = "select id,data,id_aluno,periodo,esporte,objetivo,fase from mesociclos m where m.id_aluno = ? and year(m.data) >= year (now()) group by data,id_aluno,periodo,esporte,objetivo,fase order by m.id desc";

	private $SQL_LIST_MESOCICLOS_BY_ID = "select id,data,id_aluno,periodo,esporte,objetivo,fase from mesociclos m where m.id = ? ";

	/*
		populate Mesociclo VO
	*/
	private function populateVO($rs, &$arrMesociclos) {
		
		$arrMesociclos = array();
		while (!$rs->EOF) {
			
			$arr = $rs->fetchRow();
			
			$mesociclo = new Mesociclo();
			$mesociclo->setId( $arr["id"] );
			$mesociclo->setData( $arr["data"] );
			$mesociclo->setIdAluno( $arr["id_aluno"] );
			$mesociclo->setPeriodo( $arr["periodo"] );
			$mesociclo->setEsporte( $arr["esporte"] );
			$mesociclo->setObjetivo( $arr["objetivo"] );
			$mesociclo->setFase( $arr["fase"] );

			array_push($arrMesociclos, $mesociclo);
		}


		return $arrMesociclos;
	}

	/*
		List Mesociclos, display home screen
	*/
	public function listMesociclosByAluno($idAluno) {
		$conn = new Connection();

		$rs = $conn->executeQryP($this->SQL_LIST_MESOCICLOS_BY_ALUNO, array($idAluno));
		$this->populateVO( $rs, $arrMesociclos);
		return $arrMesociclos;
	}

	/*
		List Mesociclo by ID
	*/
	public function listMesociclosById($id) {
		$conn = new Connection();

		$rs = $conn->executeQryP($this->SQL_LIST_MESOCICLOS_BY_ID, array($id));
		$this->populateVO( $rs, $arrMesociclos);
		return $arrMesociclos;
	}
}

?>