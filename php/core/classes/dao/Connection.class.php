<?php

require_once ( ADODB );

class Connection {

	private $conn;
	private $database;

	public function getConnection() {
		if (!$this->conn) {
			$this->conn = &ADONewConnection('mysql'); 
			$this->conn->PConnect(DB_HOST, DB_USER, DB_PASS, DATABASE);
		}

		return $this->conn;
	}

	public function executeQry($qry) {

		$result = $this->getConnection()->cacheExecute($qry);
		return $result;

	}

	public function executeQryP($qry, $vars) {
		$result = $this->getConnection()->Execute($qry, $vars);
		return $result;
	}

}

?>
