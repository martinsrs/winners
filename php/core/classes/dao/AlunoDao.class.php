<?php

require_once ("Connection.class.php");
require_once (VO."Aluno.class.php");

class AlunoDao {

	private $SQL_LIST_ALUNO = "select a1.*,
							 		case when exists (select 1 from alunos a2 where a2.id <> a1.id and a2.id_professor = a1.id) then 1
							 		else 0
							 		end as isAdmin
							 	from alunos a1";

	private $SQL_LIST_ALUNO_BY_ID = " where id = ? ";

	private $SQL_LIST_ALUNO_BY_PROFESSOR = "select *
											from alunos
											where id <> id_professor and id_professor = ?
												and (lower(nome) like ? or lower(sobrenome) like ?)
												and bloqueio = 0
											order by nome, sobrenome";

	private $SQL_LOGIN = "select id from alunos where usuario = ? and senha = ? and bloqueio = 0 ";

	/*
		Populate AlunoVO with RecordSet Data
	*/
	private function populateVO($resultSet, &$arrAluno) {

		$arrAluno = array();
		while (!$resultSet->EOF) {
			$aluno = new Aluno();
			$arr = $resultSet->fetchRow();

			$aluno->setId( $arr["id"] );
			$aluno->setNome( $arr["nome"] );
			$aluno->setSobrenome( $arr["sobrenome"] );
			$aluno->setNivel( $arr["nivel"] );
			$aluno->setUsuario( $arr["usuario"] );
			$aluno->setEmail( $arr["email"] );
			$aluno->setSenha( md5($arr["senha"]) );
			$aluno->setFoto( $arr["foto"] );
			$aluno->setDatanascimento( $arr["dn"] );
			$aluno->setPeso( $arr["peso"] );
			$aluno->setCidade( $arr["cidade"] );
			$aluno->setFrase( $arr["frase"] );
			$aluno->setHistoria( $arr["historia"] );
			$aluno->setIsAdmin( (isset($arr["isAdmin"])) ? $arr["isAdmin"] : 0 );

			array_push($arrAluno, $aluno);
		}
	}

	/*
		Get All Alunos
	*/
	public function listAlunos() {
		$conn = new Connection();

		$rs = $conn->executeQry($this->SQL_LIST_ALUNO);
		$this->populateVO($rs, $arrAluno);
		return $arrAluno;
	}

	/*
		Get aluno after login
	*/
	public function listAlunoById($idAluno) {
		$conn = new Connection();

		$rs = $conn->executeQryP($this->SQL_LIST_ALUNO . $this->SQL_LIST_ALUNO_BY_ID, array($idAluno));

		$this->populateVO($rs, $arrAluno);
		return $arrAluno;	
	}

	/*
		get alunos of professor
	*/
	public function listAlunosByIdProfessor($idProfessor, $nome) {
		$conn = new Connection();

		$qry = "%".$nome."%";

		$rs = $conn->executeQryP($this->SQL_LIST_ALUNO_BY_PROFESSOR, array($idProfessor,$qry,$qry));

		$this->populateVO($rs, $arrAluno);
		return $arrAluno;	
	}

	/*
		Log In
	*/
	public function login($user, $pass) {
		$conn = new Connection();

		$rs = $conn->executeQryP($this->SQL_LOGIN, array($user, $pass));
		if (!$rs->EOF) {
			$arr = $rs->fetchRow();
			return $arr["id"];
		} else {
			return false;
		}
	}

}

?>