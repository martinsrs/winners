<?php

class Aluno {

	public $id;
	public $nivel;
	public $usuario;
	public $email;
	public $senha;
	public $nome;
	public $sobrenome;
	public $foto;
	public $thumb;
	public $datanascimento;
	public $peso;
	public $marcas;
	public $cidade;
	public $frase;
	public $historia;
	public $mesociclos;
	public $isAdmin;

	public function getId() { return $this->id; }
	public function getNivel() { return $this->nivel; }
	public function getUsuario() { return $this->usuario; }
	public function getEmail() { return $this->email; }
	public function getSenha() { return $this->senha; }
	public function getNome() { return $this->nome; }
	public function getSobrenome() { return $this->sobrenome; }
	public function getFoto() { return $this->foto; }
	public function getThumb() { return $this->thumb; }
	public function getDatanascimento() { return $this->datanascimento; }
	public function getPeso() { return $this->peso; }
	public function getMarcas() { return $this->marcas; }
	public function getCidade() { return $this->cidade; }
	public function getFrase() { return $this->frase; }
	public function getHistoria() { return $this->historia; }
	public function getMesociclos() { return $this->mesociclos; }
	public function getIsAdmin() { return $this->isAdmin; }

	public function setId( $val )  { $this->id = $val; }
	public function setNivel( $val )  { $this->nivel = $val; }
	public function setUsuario( $val )  { $this->usuario = $val; }
	public function setEmail( $val )  { $this->email = $val; }
	public function setSenha( $val )  { $this->senha = $val; }
	public function setNome( $val )  { $this->nome = utf8_encode($val); }
	public function setSobrenome( $val )  { $this->sobrenome = utf8_encode($val); }
	public function setFoto( $val )  { $this->foto = $val; }
	public function setThumb( $val )  { $this->thumb = $val; }
	public function setDatanascimento( $val )  { $this->datanascimento = $val; }
	public function setPeso( $val )  { $this->peso = $val; }
	public function setMarcas( $val )  { $this->marcas = $val; }
	public function setCidade( $val )  { $this->cidade = utf8_encode($val); }
	public function setFrase( $val )  { $this->frase = utf8_encode($val); }
	public function setHistoria( $val )  { $this->historia = utf8_encode($val); }
	public function setMesociclos( $val ) { $this->mesociclos = $val; }
	public function setIsAdmin( $val ) { $this->isAdmin = $val; }

}

?>