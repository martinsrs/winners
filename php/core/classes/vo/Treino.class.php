<?php

class Treino {

	public $id;
	public $idAluno;
	public $data;
	public $modalidade;
	public $treino;
	public $volume;
	public $feedback;
	public $idMesociclo;
	public $numSemana;
	public $obs;

	public function getId() { return $this->id; }
	public function getIdAluno() { return $this->idAluno; }
	public function getData() { return $this->data; }
	public function getModalidade() { return $this->modalidade; }
	public function getTreino() { return $this->treino; }
	public function getVolume() { return $this->volume; }
	public function getFeedback() { return $this->feedback; }
	public function getIdMesociclo() { return $this->idMesociclo; }
	public function getNumSemana() { return $this->numSemana; }
	public function getObs() { return $this->obs; }

	public function setId( $val ) { $this->id = $val; }
	public function setIdAluno( $val ) { $this->idAluno = $val; }
	public function setData( $val ) { $this->data = $val; }
	public function setModalidade( $val ) { $this->modalidade = utf8_encode($val); }
	public function setTreino( $val ) { $this->treino = utf8_encode($val); }
	public function setVolume( $val ) { $this->volume = $val; }
	public function setFeedback( $val ) { $this->feedback = utf8_encode($val); }
	public function setIdMesociclo( $val ) { $this->idMesociclo = $val; }
	public function setNumSemana( $val ) { $this->numSemana = $val; }
	public function setObs( $val ) { $this->obs = utf8_encode($val); }
}

?>