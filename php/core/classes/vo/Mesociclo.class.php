<?php

class Mesociclo {
	public $id;
	public $data;
	public $idAluno;
	public $periodo;
	public $esporte;
	public $objetivo;
	public $fase;
	public $treinos;

	public function getId() 	  { return $this->id; }
	public function getData()	  { return $this->data; }
	public function getIdAluno()  { return $this->idAluno; }
	public function getPeriodo()  { return $this->periodo; }
	public function getEsporte()  { return $this->esporte; }
	public function getObjetivo() { return $this->objetivo; }
	public function getFase()     { return $this->fase; }
	public function getTreinos()  { return $this->treinos; }

	public function setId( $val ) 	    { $this->id = $val;  }
	public function setData( $val )	    { $this->data = $val; }
	public function setIdAluno( $val )  { $this->idAluno = $val; }
	public function setPeriodo( $val )  { $this->periodo = utf8_encode($val); }
	public function setEsporte( $val )  { $this->esporte = utf8_encode($val); }
	public function setObjetivo( $val ) { $this->objetivo = utf8_encode($val); }
	public function setFase( $val )     { $this->fase = utf8_encode($val); }
	public function setTreinos( $val )     { $this->treinos = $val; }

}

?>