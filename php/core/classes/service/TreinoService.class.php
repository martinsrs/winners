<?php
require_once(DAO."TreinoDao.class.php");

class TreinoService {

	private $treinoDao;

	public function listTreinosByAluno($idAluno)  {

		$this->treinoDao = new TreinoDao();

		$arrTreinos = $this->treinoDao->listTreinosByAluno($idAluno)  ;
		return $arrTreinos;
	}

	/*
		get all treinos by aluno and mesociclo	
	*/
	public function listTreinosByAlunoMesociclo($idAluno, $idMesociclo)  {

		$this->treinoDao = new TreinoDao();

		$arrTreinos = $this->treinoDao->listTreinosByAlunoMesociclo($idAluno, $idMesociclo);
		return $arrTreinos;
	}

	/*
		get last available treino by aluno
	*/
	public function getLastTreinoByAluno($idAluno)  {

		$this->treinoDao = new TreinoDao();

		$arrTreinos = $this->treinoDao->getLastTreinoByAluno($idAluno);
		return $arrTreinos;
	}

	/*
		update feedback info
	*/
	public function updateFeedBack($id, $feedback) {
		$this->treinoDao = new TreinoDao();
		return $this->treinoDao->updateFeedBack($id, $feedback);
	}

}
?>