<?php
require_once(DAO."MesocicloDao.class.php");

class MesocicloService {

	private $mesocicloDao;

	public function listMesociclosByAluno($idAluno) {

		$this->mesocicloDao = new MesocicloDao();
		$arrMesociclos = $this->mesocicloDao->listMesociclosByAluno($idAluno);

		return $arrMesociclos;
	}

	public function listMesociclosById($id) {

		$this->mesocicloDao = new MesocicloDao();
		$arrMesociclos = $this->mesocicloDao->listMesociclosById($id);

		return $arrMesociclos;
	}
}
?>