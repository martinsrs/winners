<?php

require_once(DAO."AlunoDao.class.php");

class AlunoService {

	private $alunoDao;

	public function listAlunos() {

		$this->alunoDao = new AlunoDao();
		$arrAlunos = $this->alunoDao->listAlunos();
		return $arrAlunos;
		
	}

	public function listAlunoById($idAluno) {
		$this->alunoDao = new AlunoDao();
		$arrAlunos = $this->alunoDao->listAlunoById($idAluno);

		return $arrAlunos;
	}

	public function listAlunosByIdProfessor($idProfessor, $nome) {
		$this->alunoDao = new AlunoDao();
		$arrAlunos = $this->alunoDao->listAlunosByIdProfessor($idProfessor, $nome);

		return $arrAlunos;
	}

	public function login($user, $pass) {

		$this->alunoDao = new AlunoDao();
		$login = $this->alunoDao->login($user, $pass);
		return $login;
		
	}
}

?>