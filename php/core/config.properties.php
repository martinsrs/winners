<?php
	define( "CORE"    , "" );
	define( "CLASSES" , CORE . "classes/" );
	define( "LIB"     , CORE . "lib/" );
	define( "DAO"     , CLASSES . "dao/" );
	define( "VO"      , CLASSES . "vo/" );
	define( "SERVICE" , CLASSES . "service/" );
	define( "UTIL"    , CLASSES . "util/");

	require_once(CLASSES . "config_dev.properties.php");
?>