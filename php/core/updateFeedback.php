<?php

require_once("config.properties.php");
require_once(ADODB);
require_once(UTIL."Util.class.php");
require_once(SERVICE."TreinoService.class.php");

$ret = null;

if (isset($_REQUEST["id"]) && isset($_REQUEST["feedback"])) {

	$id = $_REQUEST["id"];
	$feedback = $_REQUEST["feedback"];

	$treinoService = new treinoService();
	$v = $treinoService->updateFeedBack($id, utf8_decode($feedback));
	$ret = array('ok'=>'salvo no site');
} else {
	$ret = array('err'=>'erro ao conectar ao aplicativo');
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($ret);

?>