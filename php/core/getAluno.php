<?php

require_once("config.properties.php");
require_once(ADODB);
require_once(UTIL."Util.class.php");
require_once(SERVICE."AlunoService.class.php");

$ret = null;
$alunoService = new AlunoService();

if (isset($_REQUEST["usr"])) {

	$user = $_REQUEST["usr"];
	$ret = $alunoService->listAlunoById($user);

} else if (isset($_REQUEST["prf"]) && isset($_REQUEST["qry"])) {
	
	$prof = $_REQUEST["prf"];
	$nome = $_REQUEST["qry"];

	$ret = $alunoService->listAlunosByIdProfessor($prof, $nome);

} else {
	$ret = array('err'=>'erro ao conectar ao aplicativo');
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($ret);

?>