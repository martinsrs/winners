function Mesociclo() {

    this.getMesociclosByAluno = function (idAluno, callback) {

    	winners = new Winners();
    	var url =  winners.core + '/getMesociclo.php';

    	$.ajax({ 
	        type: "GET",
	        url: url,
	        async: true,
	        contentType: "application/json",
	        dataType: "json",
	        data: { usr: idAluno },
	        beforeSend: function() {
	        },
	        complete: function() {
	        }
	    }).done(function( data ) {
	        try { 
	            if (data.length >= 1) {
	            	return callback(data);
	            } else {
	                alert(data.err);
	            }

	        } catch (e) {
	            console.log('>' + e);
	            console.log(data); 
	        }
	    });
    }

    this.getMesocicloById = function (id, callback) {

    	winners = new Winners();
    	var url =  winners.core + '/getMesociclo.php';

    	$.ajax({ 
	        type: "GET",
	        url: url,
	        async: true,
	        contentType: "application/json",
	        dataType: "json",
	        data: { id: id },
	        beforeSend: function() {
	        },
	        complete: function() {
	        }
	    }).done(function( data ) {
	        try { 
	            if (data.length >= 1) {
	            	return callback(data);
	            } else {
	                alert(data.err);
	            }

	        } catch (e) {
	            console.log('>' + e);
	            console.log(data); 
	        }
	    });
    }

}