function Professor() {

    this.getAlunosByIdProfessor = function (idProfessor, query, callback) {

    	winners = new Winners();
    	var url =  winners.core + '/getAluno.php';

    	$.ajax({ 
	        type: "GET",
	        url: url,
	        async: true,
	        contentType: "application/json",
	        dataType: "json",
	        data: { prf: idProfessor,
	        		qry: query },
	        beforeSend: function() {
	        },
	        complete: function() {
	        }
	    }).done(function( data ) {
	        try {
	         
	            var ret = data;
	            return callback(ret);

	        } catch (e) {
	            console.log('>' + e);
	            console.log(data); 
	        }
	    });
    }

}