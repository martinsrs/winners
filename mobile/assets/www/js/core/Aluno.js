function Aluno() {

    this.getAlunoById = function (idAluno, callback) {

    	winners = new Winners();
    	var url =  winners.core + '/getAluno.php';

    	$.ajax({ 
	        type: "GET",
	        url: url,
	        async: true,
	        contentType: "application/json",
	        dataType: "json",
	        data: { usr: idAluno },
	        beforeSend: function() {
	        },
	        complete: function() {
	        }
	    }).done(function( data ) {
	        try { 
	            if (data.length >= 1) {
	            	var ret = data;
	            	if (data.length == 1) {
	            		ret = data[0];
	            	}

	            	return callback(ret);
	            } else {
	                alert(data.err);
	            }

	        } catch (e) {
	            console.log('>' + e);
	            console.log(data); 
	        }
	    });
    }

}