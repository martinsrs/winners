function Treino() {

    this.getTreinosByAlunoMesociclo = function (idAluno, idMesociclo, callback) {

    	winners = new Winners();
    	var url =  winners.core + '/getTreino.php';

    	$.ajax({ 
	        type: "GET",
	        url: url,
	        async: true,
	        contentType: "application/json",
	        dataType: "json",
	        data: { usr: idAluno, msc: idMesociclo },
	        beforeSend: function() {
	        },
	        complete: function() {

	        }
	    }).done(function( data ) {
	        try { 
	            if (data.length >= 1) {
	         		return callback(data);
	            } else {
	                alert(data.err);
	            }

	        } catch (e) {
	            console.log('>' + e);
	            console.log(data); 
	        }
	    });
    }

    this.getLastTreinoByAluno = function (idAluno, callback) {

        winners = new Winners();
        var url =  winners.core + '/getLastTreino.php';

        $.ajax({ 
            type: "GET",
            url: url,
            async: true,
            contentType: "application/json",
            dataType: "json",
            data: { usr: idAluno },
            beforeSend: function() {
            },
            complete: function() {

            }
        }).done(function( data ) {
            try {
                if (data.length >= 1) {
                    return callback(data[0]);
                } else {
                    alert(data.err);
                }

            } catch (e) {
                console.log('>' + e);
                console.log(data); 
            }
        });
    }

    this.updateFeedback = function (idTreino, feedback, callback) {
    	
    	winners = new Winners();
    	var url =  winners.core + '/updateFeedback.php';
       
        $.ajax({ 
                type: "GET",
                url: url,
                async: true,
                contentType: "application/json",
                dataType: "json",
                data: { id: idTreino, feedback: feedback },
                beforeSend: function() {
                },
                complete: function() {
                }
            }).done(function(data) {
                try { 
                    return callback(data);
                } catch (e) {
                    console.log('>' + e);
                    console.log(data);         
                }
            });
    }

}