function Winners() {

    this.host = location.protocol + '//' + location.hostname;
    //this.host = 'http://192.168.0.5'
    //this.host = 'http://equipewinners.com.br'
    this.core = this.host + '/winnerscore';

    this.logoff = function () {
    	this.destroySession();
    	this.destroyKeepAlive();

    	window.location = 'index.html';
    }

    this.login = function (user, pass, callback) {
    	console.info('winners - login');

		var url = this.core + '/login.php';
		$.ajax({ 
	        type: "GET",
	        url: url,
	        async: true,
	        contentType: "application/json",
	        dataType: "json",
	        data: { usr: user, pss: pass },
	        beforeSend: function() {
	        },
	        complete: function() {
	        }
	    }).done(function( data ) {
	        try { 
	            if (data.length == 1) {
	                $.each(data, function (i,value) {
	                    return callback(value);
	                });
	            } else {
	                alert(data.err);
	            }

	        } catch (e) {
	            console.log('>' + e);
	            console.log(data);
	        }
	    });

		return callback();
	}

	this.destroyKeepAlive = function () {
		window.localStorage.removeItem('keepAliveAluno');
	}

	this.destroySession = function () {
		window.sessionStorage.removeItem('currentAluno');
		window.sessionStorage.removeItem('currentMesociclo');
		window.sessionStorage.removeItem('currentTreino');
	}

	this.setKeepAlive = function (value) {
		window.localStorage.removeItem('keepAliveAluno');
		window.localStorage.setItem('keepAliveAluno', value);
	}

	this.getKeepAlive = function () {
		return window.localStorage.getItem('keepAliveAluno');
	}

	this.setCurrentAluno = function (value) {
		window.sessionStorage.removeItem('currentAluno');
		window.sessionStorage.setItem('currentAluno', value);
	}
	this.getCurrentAluno = function (value) {
		data = window.sessionStorage.getItem('currentAluno');

		return data;
	}

	this.setCurrentMesociclo = function(value) {
		window.sessionStorage.removeItem('currentMesociclo');
		window.sessionStorage.setItem('currentMesociclo', value);
	}

	this.getCurrentMesociclo = function(data) {
		data = window.sessionStorage.getItem('currentMesociclo');

		return data;
	}

	this.setCurrentTreino = function(value) {
		window.sessionStorage.removeItem('currentTreino');
		window.sessionStorage.setItem('currentTreino', value);
	}

	this.getCurrentTreino = function(data) {
		data = window.sessionStorage.getItem('currentTreino');

		return data;
	}
}