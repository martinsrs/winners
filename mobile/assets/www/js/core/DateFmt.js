function DateFmt() {
  this.dateMarkers = { 
    d:['getDate',function(v) { return ("0"+v).substr(-2,2)}], 
         m:['getMonth',function(v) { return ("0"+v).substr(-2,2)}],
         n:['getMonth',function(v) {
             var mthNames = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
             return mthNames[v];
           }],
         w:['getDay',function(v) {
             var dayNames = ["Segunda","Terça","Quarta","Quinta","Sexta","Sábado", "Domingo"];
             return dayNames[v];
           }],
         y:['getFullYear'],
         H:['getHours',function(v) { return ("0"+v).substr(-2,2)}],
         M:['getMinutes',function(v) { return ("0"+v).substr(-2,2)}],
         S:['getSeconds',function(v) { return ("0"+v).substr(-2,2)}],
         i:['toISOString',null]
    };

    this.format = function(date, fmt) {
    var dateMarkers = this.dateMarkers

    var dateTxt = fmt.replace(/%(.)/g, function(m, p){

        var rv = date[(dateMarkers[p])[0]]();
        if (p == 'd' || p == 'm') {
            rv = rv + 1;
        }

        if ( dateMarkers[p][1] != null ) rv = dateMarkers[p][1](rv)
        
        return rv

    });

    return dateTxt
  }
}